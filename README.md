## Notas:

    node-sass suele dar problemas al correr el proyecto en Manjaro Linux.

# Config

Tomar en cuenta en .gitlab-ci.yml parametro variables
variables:
PUBLIC_URL: https://henry-gustav-react.gitlab.io/journal-app/

Tomar en cuenta basename <Router basename="/journal-app">, para evitar pagina 401 al recargar la pagina

Tomar en cuenta "homepage": "https://henry-gustav-react.gitlab.io/journal-app", en package.json

En firebase agregar el dominio de gitlabpages (henry-gustav-react.gitlab.io)

# Links

https://jsramblings.com/continuously-deploy-a-react-app-to-gitlab-pages/

# Cloud

Cloud.henry1
journal-react
