import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import Footer from "../components/auth/Footer";
import LoginScreen from "../components/auth/LoginScreen";
import RegisterScreen from "../components/auth/RegisterScreen";

const AuthRouter = () => {
  return (
    <div className="auth__main">
      <h1 className="animate__animated animate__fadeIn">
        Generador de notas para tu dia a dia
      </h1>
      <div className="animate__animated animate__fadeInLeft auth-screen-container auth-container">
        <div className="auth__box-container">
          <Switch>
            <Route exact path="/auth/login" component={LoginScreen} />
            <Route exact path="/auth/register" component={RegisterScreen} />
            <Redirect to="/auth/login" />
          </Switch>
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default AuthRouter;
