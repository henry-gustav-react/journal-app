import { types } from '../types/types'

export const loadSpinn = () => {
  return {
    type: types.loadSpinn,

    payload: {
      status: true
    }
  }
}

export const stopSpinn = () => {
  return {
    type: types.stopSpinn,

    payload: {
      status: false
    }
  }
}
