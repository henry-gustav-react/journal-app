import { types } from '../types/types'
import { firebase, googleAuthProvider } from '../firebase/firebase-config'
import { startLoading, finishLoading } from './ui'
import { notesLogout } from './notes'

// 
export const startLoginEmailPassword = (email, password, alert) => {
  return dispatch => {
    dispatch(startLoading())

    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(({ user }) => {
        console.log(user)
        dispatch(login(user.uid, user.displayName))
        setTimeout(() => {
          dispatch(finishLoading())
        }, 3000)
      })
      .catch(e => {
        console.log(e)
        alert.error(e.message)
        dispatch(finishLoading())
        // throw e;
      })
  }
}
export const startGoogleLogin = () => {
  return dispatch => {
    firebase
      .auth()
      .signInWithPopup(googleAuthProvider)
      .then(({ user }) => {
        // console.log({user})
        dispatch(login(user.uid, user.displayName))
      })
  }
}

export const startRegisterWithEmailPasswordName = (
  email,
  password,
  name,
  alert
) => {
  return dispatch => {
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(async ({ user }) => {
        await user.updateProfile({ displayName: name })
        console.log(user)
        dispatch(login(user.uid, user.displayName))
      })
      .catch(e => {
        console.log(e)
        alert.error(e.message)
      })
  }
}

export const login = (uid, displayName) => {
  return {
    type: types.login,
    payload: {
      uid,
      displayName
    }
  }
}

export const startLogout = () => {
  return async dispatch => {
    await firebase.auth().signOut()
    dispatch(logout())
    dispatch(notesLogout())
  }
}

export const logout = () => {
  return { type: types.logout }
}
