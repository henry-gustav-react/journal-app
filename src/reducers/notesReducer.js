import { types } from '../types/types'

/*
{
    notes: [],
    
    active : null
    active: :{
        id: 'dewfwgg3gq43t',
        title: '',
        body:'', 
        imageUrl: '',
        date: 1029329433
    }
}
*/
const initialSate = {
  notes: [],
  active: null
}

export const notesReducer = (state = initialSate, action) => {
  switch (action.type) {
    case types.notesLogoutCleaning:
      return {
        ...state,
        notes: [],
        active: null
      }
    case types.notesAddNew:
      // console.log(action.payload)
      // console.log(state.notes)
      return {
        ...state,
        notes: [action.payload, ...state.notes],

        active: {
          ...action.payload
        }
      }
    case types.notesActive:
      // console.log(action.payload)
      return {
        ...state,
        active: {
          ...action.payload
        }
      }

    case types.notesLoad:
      // console.log(action.payload)
      return {
        ...state,
        notes: [...action.payload]
      }
    case types.notesUpadted:
      // console.log(action.payload)
      return {
        ...state,
        notes: state.notes.map(note =>
          note.id === action.payload.id ? action.payload.note : note
        )
      }

    case types.notesDelete:
      // console.log(action.payload)
      return {
        ...state,
        active: null,
        notes: state.notes.filter(note => note.id !== action.payload)
      }

    default:
      return state
  }
}
