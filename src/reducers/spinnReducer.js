import { types } from '../types/types'

export const spinnReducer = (state = { spinn: false }, action) => {
  switch (action.type) {
    case types.loadSpinn:
      return {
        spinn: action.payload.status
      }

    case types.stopSpinn:
      return {
        spinn: action.payload.status
      }

    default:
      return state
  }
}
