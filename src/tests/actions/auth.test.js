import configureStore from 'redux-mock-store' //ES6 modules
import thunk from 'redux-thunk'
import { login, logout } from '../../actions/auth'
import { types } from '../../types/types'
const middlewares = [thunk]
const mockStore = configureStore(middlewares)
const initState = {
  auth: {
    uid: 'testing'
  }
}
let store = mockStore(initState)
describe('Pruebas con acciones auth', () => {
  test('login y logout deben crear la accion respectiva', async () => {
    await store.dispatch(login('testing', 'Henry'))
    await store.dispatch(logout())
    const actions = store.getActions()
    console.log(actions)

    expect(actions[0].payload.displayName).toBe('Henry')
    expect(actions[1].type).toBe(types.logout)
  })
})
