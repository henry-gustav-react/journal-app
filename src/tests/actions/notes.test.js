/**
 * @jest-environment node
 */
import configureStore from 'redux-mock-store' //ES6 modules
import thunk from 'redux-thunk'
import {
  startDelete,
  startLoadingNotes,
  startNewNote,
  startSaveNotes,
  startUploading
} from '../../actions/notes'
import { db } from '../../firebase/firebase-config'
import { types } from '../../types/types'
// import { useAlert } from 'react-alert'

const middlewares = [thunk]
const mockStore = configureStore(middlewares)
const initState = {
  auth: {
    uid: 'testing'
  }
}
let store = mockStore(initState)

describe('Pruebas con las acciones de notes', () => {
  beforeEach(() => {
    store = mockStore(initState)
  })
  test('debe crear una nueva nota startnewNote ', async () => {
    await store.dispatch(startNewNote())
    const actions = store.getActions()
    // console.log(actions)
    expect(actions[0]).toEqual({
      type: types.notesAddNew,
      payload: {
        id: expect.any(String),
        title: '',
        body: '',
        date: expect.any(Number)
      }
    })
    await store.dispatch(startDelete(actions[0].payload.id))
    // const actions2 = store.getActions()
    // console.log(actions2)
  })

  test('start loading notes debe cargar las notas ', async () => {
    await store.dispatch(startLoadingNotes('testing'))
    const actions = store.getActions()
    expect(actions[0]).toEqual({
      type: types.notesLoad,
      payload: expect.any(Array)
    })

    const expected = {
      id: expect.any(String),
      title: expect.any(String),
      body: expect.any(String),
      date: expect.any(Number)
    }

    expect(actions[0].payload[0]).toMatchObject(expected)
  })

  test('startSaveNote Debe actualizar la nota', async () => {
    const note = {
      id: 'A4YMrhhmrjM3YvH1q1HK',
      title: 'titulo',
      body: 'body text!!'
    }

    await store.dispatch(startSaveNotes(note, undefined))
    const actions = store.getActions()
    console.log(actions)
    expect(actions[0].type).toBe(types.notesUpadted)
    const docRef = await db.doc(`/testing/journal/notes/${note.id}`).get()
    //  console.log(docRef.data())
    expect(docRef.data().title).toBe(note.title)
  })

 
})
