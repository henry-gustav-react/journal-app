import React from "react";

const Footer = () => {
  return (
    <div className="footer text-center">
      <div className="info">
        <p>Diseño y Construccion por Henry Tipantuña</p>
        <p>© Henry Tipantuña 2023</p>
      </div>
      <a href="https://henrygustav.github.io/henryGustav/" target="_blank" rel="noreferrer">
        Visita mi sitio
      </a>
    </div>
  );
};

export default Footer;
