import React from 'react'
import { Link } from 'react-router-dom'
import { useForm } from '../../hooks/useForm'
import validator from 'validator'
import { useDispatch, useSelector } from 'react-redux'
import { setError } from '../../actions/ui'
import { startRegisterWithEmailPasswordName } from '../../actions/auth'
import { useAlert } from 'react-alert'

const RegisterScreen = () => {
  const alert = useAlert()
  const dispatch = useDispatch()
  const { msgError } = useSelector(state => state.ui)
  // console.log(state)
  const [formValues, handleInputChange] = useForm({
    name: '',
    email: '',
    password: '',
    password2: ''
  })

  const { name, email, password, password2 } = formValues

  const handleRegister = e => {
    e.preventDefault()
   
    if (isFormValid()) {
      dispatch(startRegisterWithEmailPasswordName(email, password, name, alert))
    }
  }

  const isFormValid = () => {
    if (name.trim().length === 0) {
      dispatch(setError('Nombre es requerido'))
      return false
    } else if (email.trim().length === 0) {
      dispatch(setError('Email es requerido'))

      return false
    } else if (!validator.isEmail(email)) {
      dispatch(setError('Email incorrecto'))

      return false
    } else if (
      password !== password2 ||
      password.length < 5 ||
      email.trim().length === 0
    ) {
      dispatch(setError('password debe ser de al menos 5 caracteres'))
      return false
    }

    dispatch(setError(null))
    return true
  }
  return (
    <>
      <h3 className='auth__title'>Registro</h3>
      <form action='' onSubmit={handleRegister}>
        {msgError && <div className='auth__alert-error'>{msgError}</div>}
        <input
          type='text'
          placeholder='Name'
          name='name'
          className='auth__input'
          autoComplete='off'
          value={name}
          onChange={handleInputChange}
        />
        <input
          type='text'
          placeholder='Email'
          name='email'
          className='auth__input'
          autoComplete='off'
          value={email}
          onChange={handleInputChange}
        />
        <input
          type='password'
          placeholder='Password'
          name='password'
          className='auth__input'
          value={password}
          onChange={handleInputChange}
        />

        <input
          type='password'
          placeholder='Confirm Password'
          name='password2'
          className='auth__input'
          value={password2}
          onChange={handleInputChange}
        />
        <button type='submit' className='btn btn-primary btn-block '>
          Registrarse
        </button>
        <div className='auth__social-network '>
          <Link to='/auth/login' className='link'>
            Ya se ha registrado?
          </Link>
        </div>
      </form>
    </>
  )
}

export default RegisterScreen
