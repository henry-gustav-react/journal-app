import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { startGoogleLogin, startLoginEmailPassword } from "../../actions/auth";
import { useForm } from "../../hooks/useForm";
import { useAlert } from "react-alert";
const LoginScreen = () => {
  const alert = useAlert();
  const { loading } = useSelector((state) => state.ui);
  const dispatch = useDispatch();
  const [formValues, handleInputChange] = useForm({
    email: "henry@gmail.com",
    password: "111111",
  });
  const handleLogin = (e) => {
    e.preventDefault();
    // console.log(email, password)
    dispatch(startLoginEmailPassword(email, password, alert));
  };

  const handleGoogleLogin = () => {
    dispatch(startGoogleLogin());
  };
  const { email, password } = formValues;
  return (
    <>
      <h3 className="auth__title">Login</h3>
      <form action="" onSubmit={handleLogin}>
        <input
          type="text"
          placeholder="Email"
          name="email"
          className="auth__input"
          autoComplete="off"
          value={email}
          onChange={handleInputChange}
        />
        <input
          type="password"
          placeholder="Password"
          name="password"
          className="auth__input"
          value={password}
          onChange={handleInputChange}
        />
        <button
          type="submit"
          className="btn btn-primary btn-block"
          disabled={loading}
        >
          Login
        </button>
        <div className="auth__social-network">
          <p>Login con redes sociales</p>
          <div className="google-btn" onClick={handleGoogleLogin}>
            <div className="google-icon-wrapper">
              <img
                className="google-icon"
                src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg"
                alt="google button"
              />
            </div>
            <p className="btn-text">
              <b>Sign in with google</b>
            </p>
          </div>

          <Link to="/auth/register" className="link">
            Crear nueva cuenta
          </Link>
        </div>
      </form>
  </>
  );
};

export default LoginScreen;
