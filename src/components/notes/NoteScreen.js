import React, { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { activeNote, startDelete } from "../../actions/notes";
import { useForm } from "../../hooks/useForm";
import NotesAppBar from "./NotesAppBar";
import { Spin } from "antd";
import { LoadingOutlined } from "@ant-design/icons";
import { startSaveNotes } from "../../actions/notes";
import { useAlert } from "react-alert";
import moment from "moment";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";

const NoteScreen = () => {
  const alert = useAlert();
  const antIcon = <LoadingOutlined style={{ fontSize: 90 }} spin />;
  const dispatch = useDispatch();
  const { active: note } = useSelector((state) => state.notes);
  const noteDate = moment(note.date);
  const { spinn } = useSelector((state) => state.spinn);
  const [formValues, handleInputChange, reset] = useForm(note);
  const { body, title } = formValues;
  const aciveId = useRef(note.id);

  formValues.url = note.url;
  useEffect(() => {
    if (note.id !== aciveId.current) {
      reset(note);
      aciveId.current = note.id;
    }
  }, [note, reset]);

  useEffect(() => {
    dispatch(activeNote(formValues.id, { ...formValues }));
  }, [formValues, dispatch]);

  const handleDelete = () => {

    confirmAlert({
      title: "Borrar nota",
      message: "Esta seguro de borrar la nota ?",
      buttons: [
        {
          label: "Si",
          onClick: () => {
            dispatch(startDelete(formValues.id));
          },
        },
        {
          label: "No",
          onClick: () => {},
        },
      ],

      overlayClassName: "overlay-custom-delete",
    });
  };

  const handlePicture = () => {
    document.querySelector("#fileSelector").click();
  };
  const handleSave = () => {
    dispatch(startSaveNotes(note, alert));
  };
  return (
    <div className="notes__main-content">
      <NotesAppBar fecha={noteDate} />
      {spinn ? (
        <div style={{ textAlign: "center", paddingTop: "5rem" }}>
          <Spin indicator={antIcon} />
          <p style={{ paddingTop: "1rem" }}>CARGANDO ...</p>
        </div>
      ) : (
        <div className="notes__content">
          <div className="note_container">
            <input
              type="text"
              placeholder="Título"
              className="notes__title-input"
              autoComplete="off"
              value={title}
              name="title"
              onChange={handleInputChange}
            />

            <textarea
              placeholder="Descripción"
              cols="30"
              rows="10"
              className="notes__text-area"
              value={body}
              name="body"
              onChange={handleInputChange}
            ></textarea>

            {note.url && (
              <div className="notes__image">
                <img src={note.url} alt="image1" />
              </div>
            )}
            <div className="notes__buton_actions">
              <button className="btn btn-success" onClick={handleSave}>
                Guardar
              </button>
              <button className="btn btn-warning" onClick={handlePicture}>
                Imagen
              </button>
              <button className="btn btn-danger" onClick={handleDelete}>
                Borrar
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default NoteScreen;
